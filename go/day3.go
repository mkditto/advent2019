package main

import (
    "fmt"
	"io/ioutil"
    "strconv"
	"strings"
)

type Point struct {
	x int
	y int
}

func (p *Point) ManhattanDistance() int {
    return Abs(p.x) + Abs(p.y)
}

type Path struct {
	p []Point
}

func NewPath(instructions []string) Path {
    path := []Point{ Point { x: 0, y: 0 } }

    for _, item := range instructions {
        dir := item[0]
        m, err := strconv.ParseInt(item[1:], 10, 32)
        if err != nil {
            panic(err)
        }

        mag := int(m)
        last := path[len(path) - 1]

        switch dir {
            case 'U':
                path = append(path, Point { x: last.x, y: last.y + mag })
            case 'D':
                path = append(path, Point { x: last.x, y: last.y - mag })
            case 'L':
                path = append(path, Point { x: last.x - mag, y: last.y })
            case 'R':
                path = append(path, Point { x: last.x + mag, y: last.y })
            default:
                panic("Unknown direction")
        }
    }

    return Path { p: path }
}

func (p1 *Path) Intersections(p2 Path) []Point {
    var intersections []Point

    for i := 0; i < len(p1.p) - 1; i++ {
        for j := 0; j < len(p2.p) - 1; j++ {
            intersection := Intersects(p1.p[i], p1.p[i + 1], p2.p[j], p2.p[j + 1])
            if intersection != nil {
                intersections = append(intersections, *intersection)
            }
        }
    }

    return intersections
}

func Intersects(p1 Point, p2 Point, p3 Point, p4 Point) *Point {
    if p1.x == p2.x && p3.y == p4.y {
        if p3.y < Max(p1.y, p2.y) &&
            p3.y > Min(p1.y, p2.y) &&
            p1.x < Max(p3.x, p4.x) &&
            p1.x > Min(p3.x, p4.x) {

            return &Point { x: p1.x, y: p3.y }
        }
    } else if p3.x == p4.x && p1.y == p2.y {
        return Intersects(p3, p4, p1, p2)
    }
    return nil
}

func Input() [][]string {
	dat, err := ioutil.ReadFile("input/day3.txt")
	if err != nil {
		panic(err)
	}

	str := string(dat)
	arry := strings.Split(str[0:len(str)-1], "\n")

    var newArry [][]string
    for i := 0; i < len(arry); i++ {
        newArry = append(newArry, strings.Split(arry[i], ","))
    }

	return newArry
}

func main() {
    input := Input()

    path1 := NewPath(input[0])
    path2 := NewPath(input[1])

    inters := path1.Intersections(path2)
    intersDistance := make([]int, len(inters))
    for i, item := range inters {
        intersDistance[i] = item.ManhattanDistance()
    }

    min, minPos := MinValAndPos(intersDistance)
    fmt.Printf(
        "The closest intersection is (%v, %v), with a distance of %v\n",
        inters[minPos].x,
        inters[minPos].y,
        min,
    )
}
