package main

import (
    "math"
)

func Max(a, b int) int {
    if a > b {
        return a
    }
    return b
}

func Min(a, b int) int {
    if a < b {
        return a
    }
    return b
}

func Abs(a int) int {
    if a < 0 {
        return -a
    }
    return a
}

func MinValAndPos(arr []int) (int, int) {
    val, pos := math.MaxInt32, 0

    for i, item := range arr {
        if item < val {
            val = item
            pos = i
        }
    }

    return val, pos
}
