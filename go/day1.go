package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func input() []string {
	dat, err := ioutil.ReadFile("input/day1.txt")
	if err != nil {
		panic(err)
	}

	arry := strings.Split(string(dat), "\n")
	return arry[:len(arry) - 1]
}

func CalcFuel(mass int64, naive bool) int64 {
    fuel := (mass / 3) - 2

    if naive {
        return fuel
    }

    if fuel <= 0 {
        return 0
    }

    return fuel + CalcFuel(fuel, naive)
}

func FuelForMass(input []string, naive bool) int64 {
	res := int64(0)

	for _, num := range input {
		if n, err := strconv.ParseInt(num, 10, 64); err == nil {
			res += CalcFuel(n, naive)
		} else {
			panic(err)
		}
	}

	return res
}

func main() {
    input := input()

    fmt.Printf("The solution for part 1 : %v \n", FuelForMass(input, true))
    fmt.Printf("The solution for part 2 : %v \n", FuelForMass(input, false))
}
