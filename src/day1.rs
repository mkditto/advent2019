#![deny(rust_2018_idioms)]

use std::cmp;
use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input/day1.txt")?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)?;

    let input = &contents.lines().collect::<Vec<&str>>();

    let part1: i32 = input
        .into_iter()
        .map(|x| {
            let x: i32 = x.parse().unwrap();

            cmp::max((x / 3) - 2, 0)
        })
        .sum();

    let part2: i32 = input
        .into_iter()
        .map(|x| {
            fn calc_fuel(mass: i32) -> i32 {
                let fuel = (mass / 3) - 2;

                if fuel <= 0 {
                    return 0;
                }
                fuel + calc_fuel(fuel)
            }

            calc_fuel(x.parse::<i32>().unwrap())
        })
        .sum();

    println!("The solution for part 1 : {}", part1);
    println!("The solution for part 2 : {}", part2);

    Ok(())
}
