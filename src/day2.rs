#![deny(rust_2018_idioms)]

mod intcode_machine;

use std::fs::File;
use std::io::prelude::*;

use intcode_machine::IntcodeMachine;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input/day2.txt")?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)?;

    let input : Vec<&str> = contents[0 .. contents.len() - 1]
        .split(",")
        .collect();

    let mut machine = IntcodeMachine::new(input);

    // Solution part 1
    machine.touch(1, 12);
    machine.touch(2, 2);
    machine.run();

    println!("The result at addr 0 : {}", machine.peek(0));

    // Solution part 2
    for i in 0..99 {
        for j in 0..99 {
            machine.reset();
            machine.touch(1, i);
            machine.touch(2, j);
            machine.run();

            if machine.peek(0) == 19690720 {
                println!("noun : {}, verb : {}", i, j);
            }
        } 
    }

    Ok(())
}
