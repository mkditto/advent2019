/// A structure to represent the machine state of an intcode machine
#[derive(Debug)]
pub struct IntcodeMachine {
    halt : bool,
    pc   : usize,
    mem  : Vec<i32>,
    rom  : Vec<i32>,
}

impl IntcodeMachine {
    /// Create new machine with memory populated by some vector
    pub fn new(input : Vec<&str>) -> IntcodeMachine {
        let input : Vec<i32> = input
            .iter()
            .map(|x| x.parse::<i32>().unwrap())
            .collect();

        IntcodeMachine {
            halt : false,
            pc   : 0,
            mem  : input.clone(),
            rom  : input,
        }
    }

    /// Get the value in memory at addr.
    /// Equivalent to:
    /// ```
    /// memory[addr]
    /// ```
    pub fn peek(&self, addr : usize) -> i32 {
        self.mem[addr]
    }

    /// Change a value in memory at addr to val.
    /// Equivalent to:
    /// ```
    /// memory[addr] = val
    /// ```
    pub fn touch(&mut self, addr : usize, val : i32) {
        self.mem[addr] = val;
    }

    /// Decode the params for an instruction into a tuple
    pub fn decode(&self) -> (usize, usize, usize) {
        (self.mem[self.pc + 1] as usize, self.mem[self.pc + 2] as usize, self.mem[self.pc + 3] as usize)
    }

    /// Fetch, decode, and execute
    pub fn fde(&mut self) {
        let cmd = self.mem[self.pc];

        match cmd {
            1  => {
                let (n1, n2, dest) = self.decode();
                
                self.mem[dest] = self.mem[n1] + self.mem[n2];
                self.pc += 4;
            },
            2  => {
                let (n1, n2, dest) = self.decode();

                self.mem[dest] = self.mem[n1] * self.mem[n2];
                self.pc += 4;
            },
            99 => self.halt = true,
            _  => panic!("unexpected instruction"),
        }
    }

    /// Run the machine until a halt is reached
    pub fn run(&mut self) {
        while !self.halt {
            self.fde();
        }
    }

    /// Resets the machine to its initial memory state. Allows for multiple runs
    /// with the same input without having to run the binary twice.
    pub fn reset(&mut self) {
        self.pc = 0;
        self.halt = false;
        self.mem = self.rom.clone();
    }
}
